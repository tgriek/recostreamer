import kafka.producer._
import java.util.Properties
import org.apache.spark._
import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.streaming.kafka._

object TextStreamer {
	def main(args: Array[String]){
		val conf = new SparkConf().setMaster("local[2]").setAppName("FileStream")
		val ssc = new StreamingContext(conf, Seconds(5))
		val topics = "tracker"
		val topicMap = topics.split(",").map((_,1)).toMap
		val lines = KafkaUtils.createStream(ssc, "54.171.77.17:2181", "tracker", topicMap)
		lines.print()
		ssc.start()
		ssc.awaitTermination()	
	}		
}

