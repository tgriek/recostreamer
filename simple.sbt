name := "Text Streamer"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.spark" %% "spark-streaming" % "1.0.2"

libraryDependencies += "org.spark-project" % "spark-core_2.9.3" % "0.7.3"


libraryDependencies += "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.0.2"

resolvers ++= Seq(
	"Akka Repo" at "http://repo.akka.io/releases/",
	"Spray " at "http://repo.spray.cc/"
)
